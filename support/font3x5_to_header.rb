#!/usr/bin/env ruby
# Takes in an ASCII font sprite sheet and outputs C header code suitable for
# use by print3x5_string() inside of font3x5.cpp
# Author: Alex Gittemeier <S529115@nwmissouri.edu>

require 'chunky_png'

USAGE = <<-EOF
Command line arguments missing or wrong
Usage: #{$0} <font3x5.png>
EOF

WIDTH = 4
HEIGHT = 6

raise USAGE if ARGV.empty?
filename = ARGV.shift
raise USAGE if !ARGV.empty?

font_sheet = ChunkyPNG::Image.from_file(filename)
raise "Sheet width doesn't have exactly #{WIDTH} wide cells" if font_sheet.width % WIDTH != 0
raise "Sheet width doesn't have exactly #{HEIGHT} tall cells" if font_sheet.height % HEIGHT != 0

puts "const uint16_t FONT[] = {  // Generated via font3x5_to_header.rb with #{filename} as an input"

(font_sheet.height / HEIGHT).times do |cell_y|
	print "\t"
	(font_sheet.width / WIDTH).times do |cell_x|
		d = []
		(cell_y * HEIGHT...(cell_y+1) * HEIGHT).each do |y|
			(cell_x * WIDTH...(cell_x+1) * WIDTH).each do |x|
				c = font_sheet[x, y]
				r, g, b = [(c >> 24) & 0xFF, (c >> 16) & 0xFF, (c >> 8) & 0xFF]
				d << (r + g + b > 127 * 3 ? 0 : 1);
			end
		end

		#based specifically on a 4x6 grid as shown:
		#
		# 012-
		# 345-
		# 678-
		# 9AB-
		# CDE-
		# -F-x
		#
		# Pixels with '-' are implied as zero
		# The pixel with 'x' is implied as zero except for the letter Q (a special case not encoded in the emitted array)
		packed = 0
		[0,  1,  2,
		 4,  5,  6,
		 8,  9, 10,
		12, 13, 14,
		16, 17, 18,
				21].each_with_index do |i, shift|
			packed |= (d[i] << shift)
		end

		[3, 7, 11, 15, 19, 20, 22, 23].each do |i|
			$stderr.puts "warn: #{[cell_x, cell_y]}: position #{i} is set" if d[i] != 0 && !(cell_x == 1 && cell_y == 5) 
		end

		print "0x%04x, " % packed
	end
	puts
end

puts "};"
