/*
 * a 3x5 size font that prints wholly within a 4x6 cell (exactly filling the OLED shield with 16x8 characters)
 * Author: Alex Gittemeier <S529115@nwmissouri.edu>
 */

#ifndef FONT3x5_H
#define FONT3x5_H

#include "SparkFunMicroOLED.h"

void font3x5_print(MicroOLED *oled, const char *str, int x = -1, int y = -1, int wrap_start = 0, int wrap_end = -1);
void font3x5_position(MicroOLED *oled, int x = -1, int y = -1, int wrap_start = 0, int wrap_end = -1);

__attribute__((format(printf, 2, 3)))
void font3x5_printf(MicroOLED *oled, const char *fmt, ...);

#endif
