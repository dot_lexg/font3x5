/*
 * a 3x5 size font that prints wholly within a 4x6 cell (exactly filling the OLED shield with 16x8 characters)
 * Author: Alex Gittemeier <S529115@nwmissouri.edu>
 */

#include <stdarg.h>
#include "font3x5.h"

void font3x5_print(MicroOLED *oled, const char *str, int x /*= -1*/, int y /*= -1*/, int wrap_start /*= 0*/, int wrap_end /*= -1*/) {
  const uint16_t FONT[] = {  // Generated via font3x5_to_header.rb with font3x5.png as an input
    0x0000, 0x1049, 0x002d, 0x5f7d, 0xb9ce, 0x42a1, 0xe28e, 0x0012, 0x4494, 0x1491, 0x0155, 0x05d0, 0x1200, 0x01c0, 0x2000, 0x12a4,
    0x2b6a, 0x749a, 0x72a3, 0x38a3, 0x49e9, 0x38cf, 0x7bce, 0x252f, 0x7bef, 0x39ef, 0x0208, 0x1208, 0x4454, 0x0e38, 0x1511, 0x20a3,
    0x636e, 0x5bea, 0x3aeb, 0x624e, 0x3b6b, 0x72cf, 0x12cf, 0x6b4e, 0x5bed, 0x7497, 0x3927, 0x5aed, 0x7249, 0x5bfd, 0x5b6f, 0x7b6f,
    0x12eb, 0x7b6f, 0x5aeb, 0x39ce, 0x2497, 0x6b6d, 0x256d, 0x5fed, 0x5aad, 0x39ed, 0x72a7, 0x6496, 0x4889, 0x3493, 0x002a, 0x7000,
    0x0111, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
    0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x64d6, 0x2492, 0x3593, 0x00aa
  };
  const char FONT_RANGEMIN = ' ', FONT_RANGEMAX = 0x7E;

  static int cursor_x = 0, cursor_y = 0;
  if (x >= 0) cursor_x = x;
  if (y >= 0) cursor_y = y;
  if (wrap_end < 0) wrap_end += oled->getLCDWidth();

  for (; *str != '\0'; str++) {
    char c = toupper(*str);

    //Handle linewrapping
    if (cursor_x + 3 > wrap_end || c == '\n') {
      cursor_x = wrap_start;
      cursor_y += 6;
      if (c == '\n')
        continue;
      if (c == ' ' && x == -1) //if an autowrap happens, eat the leading space
        continue;
    }

    if (cursor_y + 5 >= oled->getLCDHeight())
      break;

    // font decoded on a 4x6 grid as shown:
    //
    //  012-
    //  345-
    //  678-
    //  9AB-
    //  CDE-
    //  -F-x
    //
    // Where - is always unset, and x is set only for 'Q'

    uint16_t d = (c >= FONT_RANGEMIN && c <= FONT_RANGEMAX) ? FONT[c - FONT_RANGEMIN] : 0;
    if (d & 0x0001) oled->pixel(cursor_x + 0, cursor_y + 0);
    if (d & 0x0002) oled->pixel(cursor_x + 1, cursor_y + 0);
    if (d & 0x0004) oled->pixel(cursor_x + 2, cursor_y + 0);
    if (d & 0x0008) oled->pixel(cursor_x + 0, cursor_y + 1);
    if (d & 0x0010) oled->pixel(cursor_x + 1, cursor_y + 1);
    if (d & 0x0020) oled->pixel(cursor_x + 2, cursor_y + 1);
    if (d & 0x0040) oled->pixel(cursor_x + 0, cursor_y + 2);
    if (d & 0x0080) oled->pixel(cursor_x + 1, cursor_y + 2);
    if (d & 0x0100) oled->pixel(cursor_x + 2, cursor_y + 2);
    if (d & 0x0200) oled->pixel(cursor_x + 0, cursor_y + 3);
    if (d & 0x0400) oled->pixel(cursor_x + 1, cursor_y + 3);
    if (d & 0x0800) oled->pixel(cursor_x + 2, cursor_y + 3);
    if (d & 0x1000) oled->pixel(cursor_x + 0, cursor_y + 4);
    if (d & 0x2000) oled->pixel(cursor_x + 1, cursor_y + 4);
    if (d & 0x4000) oled->pixel(cursor_x + 2, cursor_y + 4);
    if (d & 0x8000) oled->pixel(cursor_x + 1, cursor_y + 5);
    if (c == 'Q')   oled->pixel(cursor_x + 3, cursor_y + 5); //Special case

    cursor_x += 4;
		x = -1; //unset x so that autowrap detection works properly
  }
}

void font3x5_position(MicroOLED *oled, int x /* = -1 */, int y /* = -1 */, int wrap_start /* = 0 */, int wrap_end /* = -1 */) {
  //XXX: this is a hack, really font3x5 should be a proper class so we could expose cursor_x/y to the client if they needed it
  font3x5_print(oled, "", x, y, wrap_start, wrap_end);
}

//BUGS: a vprintf is not included ... but since this a shortcut anyway, I think that's okay.
void font3x5_printf(MicroOLED *oled, const char *fmt, ...) {
  const int BUFFER_SZ = 1025; //The max size string this display could possibly hold + 1 character for \0
  static char buffer[BUFFER_SZ];
  va_list args;
  va_start(args, fmt);
  vsnprintf(buffer, BUFFER_SZ, fmt, args);
  va_end(args);
  font3x5_print(oled, buffer);
}
