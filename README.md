# font3x5

Font rendering library for an extremely tiny font targeted at the
SparkFunMicroOLED library. The font works on a 4x6 grid but most
glyphs fit in a 3x5 cell.

With this font you can fit 8 lines of text, each 16 characters wide. This is a
huge improvement over the 5x7 (on a 5x8 grid) font included in the
SparkFunMicroOLED library which can only print 6 lines, each 12 characters wide.

## Include it

Inside the /lib/ folder of a Particle project, run `git clone https://github.com/win93/font3x5`.
(You can alternatively copy the contents of /src/ into your project.)

Then add:

    #include "font3x5.h"

## Use it

Assuming you have a `MicroOLED oled;` in scope:

	oled.clear(PAGE);
	font3x5_print(&oled, "Hello world\n", 0, 0); //print hello world at X=0, Y=0 (coordinates specify top-left of first character)
	font3x5_print(&oled, "More text"); //print continuing where the last text left off
	oled.display();

## Modify it

Make sure you have a working ruby interpreter. Make changes to `font3x5.png` then run

	gem install chunky_png #Unless you already have this dependency
	ruby support/font3x5_to_header.rb support/font3x5.png

Then paste the output into the implmenetation of `font3x5()`, in src/font3x5.cpp.
You must delete the first two rows of `FONT[]` or adjust `FONT_RANGEMIN` to `'\0'`.
You can also delete the very last index (which corresponds to the `'\x7F'` or
ASCII DEL character), or adjust `FONT_RANGEMAX` accordingly.

## Bugs

 - Only uppercase characters are supported.
 - Only the middle bottom pixel is allowed to be specified in the sprite sheet;
   the bottom-right tail if the "Q" is hard coded in the font rendering routine.
